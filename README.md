# Simple CMS

Simple CMS built on Ruby on Rails with support for rich text editing with Trix Editor/Active Text (Rails 6). **[Toy Project]**.

Also supports authentication with Devise.

Ruby 2.6.6

Rails 6.
